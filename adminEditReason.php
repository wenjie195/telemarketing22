<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Reason.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE user_type =1 ");

// $statusDetails = getStatus($conn);
// $reasonDetails = getReason($conn);

$companyDetails = getReason($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Edit Reason | adminTele" />
    <title>Edit Reason | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <!-- <h1 class="h1-title">Edit Company</h1> -->

    <h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Edit Reason
        </a>
    </h1>

    <div class="clear"></div>

    <?php
    if(isset($_POST['reason']))
    {
    $conn = connDB();
    $reasonDetails = getReason($conn,"WHERE reason = ? ", array("reason") ,array($_POST['reason']),"s");
    ?>

    <form  action="utilities/editReasonFunction.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Reason : <?php echo $reasonDetails[0]->getReasonA();?></p>
            <input class="clean tele-input" type="text" placeholder="New Reason" id="edit_reason_name" name="edit_reason_name" required>        
        </div> 

        <div class="clear"></div>

        <input type="hidden" value="<?php echo $reasonDetails[0]->getId();?>" id="reason_id" name="reason_id" readonly>        

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

    </form>

    <?php
    }
    ?>

</div>
<style>
.statusreason-li{
	color:#bf1b37;
	background-color:white;}
.statusreason-li .hover1a{
	display:none;}
.statusreason-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>