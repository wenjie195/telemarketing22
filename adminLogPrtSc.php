<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/Printscreen.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $pageView = getPageview($conn, "ORDER BY date_created DESC");
$pageView = getPrintscreen($conn, "ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<meta property="og:title" content="Log (Prt Sc) | adminTele" />
<title>Log (Prt Sc)  | adminTele</title>

<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"> </script>'; ?> -->
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

  <!-- <h1 class="h1-title customer-h1">Customer</h1> -->
  <h4 class="h1-title customer-h1"><a href="adminLogLogin.php" class="red-link">Log (Login)</a> | Log (Prt Sc) </h4>

  <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table" id="myTable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Staff Name</th>
                            <th>Page</th>
                            <th>Date</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $conn = connDB();
                        if($pageView)
                        {   
                            for($cnt = 0;$cnt < count($pageView) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>

                                    <td>
                                    <?php 
                                        $userUid = $pageView[$cnt]->getUserUid();

                                        $conn = connDB();
                                        $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($userUid),"s");
                                        echo $username = $userDetails[0]->getUsername();
                                    ?>
                                    </td>

                                    <td><?php echo $pageView[$cnt]->getPage();?></td>
                                    <td><?php echo $pageView[$cnt]->getDateCreated();?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        <?php
                        }
                        $conn->close();
                        ?>
                    </tbody>

                </table>
            </div>
    </div>
        
</div>

<style>
.log-li{
	color:#bf1b37;
	background-color:white;}
.log-li .hover1a{
	display:none;}
.log-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/hot-sneaks/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

</body>
</html>