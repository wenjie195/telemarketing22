<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Reason.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE user_type =1 ");

$statusDetails = getStatus($conn);
// $reasonDetails = getReason($conn);
$reasonDetails = getReason($conn," WHERE type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Status and Reason | adminTele" />
    <title>Status and Reason | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title">Status and Reason</h1>

    <div class="clear"></div>

    <div class="width100 extra-m-btm">
    	<button class="clean red-btn margin-top30 fix300-btn add-status-btn text-center open-status">Add Status/Reason</button>
    </div>

    <!-- Status Modal -->
    <div id="status-modal" class="modal-css">
        <!-- Modal content -->
        <div class="modal-content-css status-content">
        <span class="close-css close-status">&times;</span>
        <h2 class="h2-title text-center">Add Status/Reason</h2>
        <form action="utilities/addStatusReasonFunction.php" method="POST">
            <p class="input-p">Status</p>
            <select class="clean tele-input white-bg-input-ow" id="add_status" name="add_status" required>
            <option value="">Select a Status</option>
                <?php 
                for ($cntPro=0; $cntPro <count($statusDetails) ; $cntPro++)
                {
                ?>
                <option value="<?php echo $statusDetails[$cntPro]->getStatus();?>"> 
                <?php echo $statusDetails[$cntPro]->getStatus(); ?>
                </option>
                <?php
                }
                ?>
            </select>
            <p class="input-p">Reason</p>
            <input class=" tele-input white-bg-input-ow clean" type="text" placeholder="Reason" id="add_reason" name="add_reason" required>           
            
            <div class="clear"></div>
            <div class="width100 text-center">
                <button class="clean red-btn text-center modal-submit">Submit</button>
            </div>
        </form>
        </div>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>STATUS</th>
                            <th>REASON</th>
                            <th>DATE CREATED</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($reasonDetails)
                        {   
                            for($cnt = 0;$cnt < count($reasonDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $reasonDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $reasonDetails[$cnt]->getReasonA();?></td>
                                <td><?php echo $reasonDetails[$cnt]->getDateCreated();?></td>
                                <td>
                                    <form action="adminEditReason.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="reason" value="<?php echo $reasonDetails[$cnt]->getReasonA();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <!-- <form action="adminEditReason.php" method="POST"> -->
                                    <form  action="utilities/deleteReasonFunction.php" method="POST">
                                        <input type="hidden" value="<?php echo $reasonDetails[$cnt]->getId();?>" id="reason_id" name="reason_id" readonly>   
                                        <button class="clean hover1 img-btn" type="submit" name="delete_reason" value="<?php echo $reasonDetails[$cnt]->getReasonA();?>">
                                            <img src="img/delete.png" class="width100 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete2.png" class="width100 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
            </div>
    </div>
</div>
<style>
.statusreason-li{
	color:#bf1b37;
	background-color:white;}
.statusreason-li .hover1a{
	display:none;}
.statusreason-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
</body>
</html>