<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/TimeTeleUpdate.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
// $teleName = $userDetails -> getUsername();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Edit Profile | adminTele" />
    <title>Edit Profile | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Edit Profile</h1> 

        <!-- <form action="utilities/addNewRefereeFunction.php" method="POST"> -->
        <form action="utilities/editProfileFunction.php" method="POST">

        <div class="input50-div">
			<p class="input-title-p">Name</p>
            <input class="clean tele-input no-input-style" type="text" value="<?php echo $userDetails->getUsername();?>" readonly>        
        </div> 

        <div class="input50-div second-input50">
			<p class="input-title-p">Email</p>
            <input class="clean tele-input"  type="email" placeholder="Email" value="<?php echo $userDetails->getEmail();?>" id="update_email" name="update_email" required>        
        </div>
        
        <div class="clear"></div>

        <div class="input50-div">
        	<p class="input-title-p">Address</p>
            <input class="clean tele-input"  type="text" placeholder="Address" value="<?php echo $userDetails->getAddress();?>" id="update_address" name="update_address" required>  
        </div>

        <div class="input50-div second-input50">
        	<p class="input-title-p">Contact</p>
            <input class="clean tele-input"  type="text" placeholder="Contact" value="<?php echo $userDetails->getPhoneNo();?>" id="update_phone" name="update_phone" required>  
        </div>         

        <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>
        </form>

       
</div>
<style>
.editpro-li{
	color:#bf1b37;
	background-color:white;}
.editpro-li .hover1a{
	display:none;}
.editpro-li .hover1b{
	display:block;}
</style>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "注册新用户失败！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>
<?php include 'js.php'; ?>
</body>
</html>