<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/TimeTeleUpdate.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
// $teleName = $userDetails -> getUsername();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Edit Password | adminTele" />
    <title>Edit Password | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Edit Password</h1> 

        <!-- <form action="#" method="POST"> -->
        <form action="utilities/editPasswordFunction.php" method="POST">

        <div class="input50-div">
			<p class="input-title-p">Current Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="Current Password" id="current_password" name="current_password" required>        	
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
            </div>
        </div>
        
        <div class="clear"></div>

        <div class="input50-div">
        	<p class="input-title-p">New Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="New Password" id="new_password" name="new_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>   
        </div>

        <div class="input50-div second-input50">
        	<p class="input-title-p">Retype New Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
            </div>
        </div>         

        <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>
        </form>

       
</div>
<style>
.password-li{
	color:#bf1b37;
	background-color:white;}
.password-li .hover1a{
	display:none;}
.password-li .hover1b{
	display:block;}
</style>

<script>
function myFunctionA()
{
    var x = document.getElementById("current_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

<?php include 'js.php'; ?>
</body>
</html>