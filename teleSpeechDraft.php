<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Notepad.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$teleName = $userDetails -> getUsername();

$notepadData = getNotepad($conn," WHERE author_uid = ? AND type = '1' ",array("author_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Speech Draft | adminTele" />
    <!-- <meta http-equiv="refresh" content="<?php //echo $sec?>;URL='<?php //echo $page?>'"> -->
    <title>Speech Draft | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title">Speech Draft</h1>
    
    <div class="clear"></div>

    <div class="width100 extra-m-btm">
    	<a href="teleAddSpeech.php">
			<div class="clean red-btn margin-top30 fix300-btn add-status-btn text-center open-status">New Draft</div>
		</a>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>TITLE</th>
                            <th>DATE</th>
                            <th>VIEW</th>
                            <!-- <th>DOWNLOAD</th> -->
                            <th>EDIT</th>
                            <th>DELETE</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($notepadData)
                        {   
                            for($cnt = 0;$cnt < count($notepadData) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $notepadData[$cnt]->getTitle();?></td>
                                <td><?php echo $notepadData[$cnt]->getDateCreated();?></td>
                                <!-- <td>View</td> -->

                                <td>
                                    <form action="teleViewSpeech.php" method="POST">
                                        <button class="clean img-btn hover1" name="notepad_uid" value="<?php echo $notepadData[$cnt]->getUid();?>">
                                            <img src="img/view.png" class="width100 hover1a edit-icon" title="View" alt="View">
                                            <img src="img/view2.png" class="width100 hover1b edit-iconb" alt="View" title="View">
                                        </button>
                                    </form>
                                </td>

                                <!-- <td>
                                    <button class="clean img-btn hover1">
                                        <img src="img/download.png" class="width100 hover1a edit-icon" title="Download" alt="Download">
                                        <img src="img/download2.png" class="width100 hover1b edit-iconb" alt="Download" title="Download">
                                    </button>
                                </td> -->

                                <td>
                                    <form action="teleEditSpeech.php" method="POST">
                                        <button class="clean hover1 img-btn" name="notepad_uid" value="<?php echo $notepadData[$cnt]->getUid();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <form action="utilities/deleteNotesFunction.php" method="POST">
                                        <button class="clean img-btn hover1" name="notepad_uid" value="<?php echo $notepadData[$cnt]->getUid();?>">
                                            <img src="img/delete.png" class="width100 hover1a edit-icon" title="Delete" alt="Delete">
                                            <img src="img/delete2.png" class="width100 hover1b edit-iconb" alt="Delete" title="Delete">
                                        </button>	
                                    </form>					   	
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                    <!-- <tbody>
                       <tr>
						   <td>1.</td>
						   <td>Daily Speech</td>
						   <td>10/08/2020</td>
						   <td><a href="teleViewSpeech.php">
							   	<button class="clean img-btn hover1">
							    	<img src="img/view.png" class="width100 hover1a edit-icon" title="View" alt="View">
                                    <img src="img/view2.png" class="width100 hover1b edit-iconb" alt="View" title="View">
							  	</button>
							   </a>
						   </td>
						   <td>
							   	<button class="clean img-btn hover1">
							    	<img src="img/download.png" class="width100 hover1a edit-icon" title="Download" alt="Download">
                                    <img src="img/download2.png" class="width100 hover1b edit-iconb" alt="Download" title="Download">
							  	</button>
						   </td>						   
						   <td><a href="teleEditSpeech.php">
							   	<button class="clean img-btn hover1">
							    	<img src="img/edit2.png" class="width100 hover1a edit-icon" title="Edit" alt="Edit">
                                    <img src="img/edit3.png" class="width100 hover1b edit-iconb" alt="Edit" title="Edit">
							  	</button>
							   </a>
						   </td>
						   <td>
							   	<button class="clean img-btn hover1">
							    	<img src="img/delete.png" class="width100 hover1a edit-icon" title="Delete" alt="Delete">
                                    <img src="img/delete2.png" class="width100 hover1b edit-iconb" alt="Delete" title="Delete">
							  	</button>						   	
						   </td>
					   </tr>
                    </tbody> -->

                </table>
            </div>
    </div>

        
</div>

<style>
.speech-li{
	color:#bf1b37;
	background-color:white;}
.speech-li .hover1a{
	display:none;}
.speech-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>