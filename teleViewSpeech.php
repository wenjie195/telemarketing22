<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Notepad.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
// $teleName = $userDetails -> getUsername();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="View Speech Draft | adminTele" />
    <title>View Speech Draft | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>

<div class="next-to-sidebar">

	<div class="mobile-font">
	<h1 class="h1-title">View Speech Draft</h1> 

    <?php
    if(isset($_POST['notepad_uid']))
    {
    $conn = connDB();
    $notepadDetails = getNotepad($conn,"WHERE uid = ? ", array("uid") ,array($_POST['notepad_uid']),"s");
    ?>

        <div class="width100">
			<p class="input-title-p">Title</p>
            <input class="clean tele-input no-input-style" type="text" value="<?php echo $notepadDetails[0]->getTitle();?>" readonly>        
        </div> 

		<div class="clear"></div>


        <div class="width100">
			<p class="input-title-p">Content</p>
			<textarea class="clean tele-input no-input-style content-display" readonly><?php echo $notepadDetails[0]->getContent();?></textarea>        
        </div>       

        <div class="clear"></div>

        <a href="teleSpeechDraft.php">
            <div class="clean red-btn margin-top30 fix300-btn text-center" name="submit">Back</div>
        </a>

    <?php
    }
    ?>

        <!-- <div class="width100">
			<p class="input-title-p">Title</p>
            <input class="clean tele-input no-input-style" type="text" placeholder="Title">        
        </div> 
		<div class="clear"></div>


        <div class="width100">
			<p class="input-title-p">Content</p>
			<textarea class="clean tele-input no-input-style" placeholder="Speech Content"></textarea>        
        </div>       

        <div class="clear"></div>

        <a href="teleSpeechDraft.php">
            <div class="clean red-btn margin-top30 fix300-btn text-center" name="submit">Back</div>
        </a> -->

	</div> 

       
</div>

<style>
.speech-li{
	color:#bf1b37;
	background-color:white;}
.speech-li .hover1a{
	display:none;}
.speech-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>