<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Notepad.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

$userUid = $_SESSION['uid'];

function addNewNotes($conn,$uid,$title,$content,$authorName,$authorUid,$type)
{
     if(insertDynamicData($conn,"notepad",array("uid","title","content","author_name","author_uid","type"),
     array($uid,$title,$content,$authorName,$authorUid,$type),"sssssi") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $title = rewrite($_POST["title"]);
     $content = rewrite($_POST["content"]);

     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
     $authorName = $userRows[0]->getUsername();
     $authorUid = $userRows[0]->getUid();

     $type = 1;

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $status."<br>";

     if(addNewNotes($conn,$uid,$title,$content,$authorName,$authorUid,$type))
     {
          header('Location: ../teleSpeechDraft.php');
     }
     else
     {
          header('Location: ../teleSpeechDraft.php?type=2');
     }
}
else
{
     header('Location: ../index.php');
}
?>