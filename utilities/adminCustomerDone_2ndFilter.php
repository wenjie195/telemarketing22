<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/SecondCustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function secondFilter($conn,$teleName,$customerName,$customerPhone,$remark,$type,$reason,$companyName,$remarkB,$occupation,$hobby,$location)
{
     if(insertDynamicData($conn,"second_customer_details",array("tele_name","name","phone","remark","type","reason","company_name","remark_two","occupation","hobby","location"),
     array($teleName,$customerName,$customerPhone,$remark,$type,$reason,$companyName,$remarkB,$occupation,$hobby,$location),"sssssssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $phone = $_POST["customer_phone"];
     $action = "Done";

     $customerPhone = $phone;
     $teleName = rewrite($_POST["tele_pic"]);
 
     $ctmDetails = getCustomerDetails($conn," WHERE phone = ? ",array("phone"),array($customerPhone),"s");
     $customerName = $ctmDetails[0]->getName();
     $remark = $ctmDetails[0]->getRemark();
     $companyName = $ctmDetails[0]->getCompanyName();
     $type = $ctmDetails[0]->getType();
     $reason = $ctmDetails[0]->getReason();
     $remarkB = $ctmDetails[0]->getRemarkB();
     $occupation = $ctmDetails[0]->getOccupation();
     $hobby = $ctmDetails[0]->getHobby();
     $location = $ctmDetails[0]->getLocation();

     // $action = "Reject";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $id."<br>";
     // echo $reasonName."<br>";


     $customerDetails = getCustomerDetails($conn," phone = ?  ",array("phone"),array($phone),"s");    

     if(!$customerDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($action)
          {
               array_push($tableName,"action");
               array_push($tableValue,$action);
               $stringType .=  "s";
          }

          array_push($tableValue,$phone);
          $stringType .=  "s";
          $updatedReason = updateDynamicData($conn,"customerdetails"," WHERE phone = ? ",$tableName,$tableValue,$stringType);
          if($updatedReason)
          {
               // echo "success";
               // echo "<script>alert('Update Action as Done');window.location='../checkLogGood.php'</script>";   

               if(secondFilter($conn,$teleName,$customerName,$customerPhone,$remark,$type,$reason,$companyName,$remarkB,$occupation,$hobby,$location))
               {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../profile.php?type=1');
                    // echo "<script>alert('Register Success !');window.location='../checkLogGood.php'</script>";    

                    if($type == 'Good')
                    {
                         // $_SESSION['messageType'] = 1;
                         // header('Location: ../profile.php?type=1');
                         echo "<script>alert('Updated !');window.location='../checkLogGood.php'</script>";    
                    }
                    else
                    {
                         echo "<script>alert('Updated !');window.location='../checkLogUpdated.php'</script>"; 
                    }

               }
               else
               {
                   echo "<script>alert('FAIL !!');window.location='../checkLogGood.php'</script>";
               }

          }
          else
          {
               // echo "fail to update";
               echo "<script>alert('fail to update action !!');window.location='../checkLogGood.php'</script>";   
          }
     }
     else
     {
          // echo "GG";
          echo "<script>alert('ERROR !!');window.location='../checkLogGood.php'</script>";   
     }
}
else 
{
     header('Location: ../index.php');
}
?>