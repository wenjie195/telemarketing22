<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/SecondCustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

function secondFilter($conn,$teleName,$customerName,$customerPhone,$remark,$type,$reason,$companyName,$remarkB,$occupation,$hobby,$location)
{
     if(insertDynamicData($conn,"second_customer_details",array("tele_name","name","phone","remark","type","reason","company_name","remark_two","occupation","hobby","location"),
     array($teleName,$customerName,$customerPhone,$remark,$type,$reason,$companyName,$remarkB,$occupation,$hobby,$location),"sssssssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $customerPhone = rewrite($_POST["phone_no"]);
    $teleName = rewrite($_POST["tele_pic"]);

    $ctmDetails = getCustomerDetails($conn," WHERE phone = ? ",array("phone"),array($customerPhone),"s");
    $customerName = $ctmDetails[0]->getName();
    $remark = $ctmDetails[0]->getRemark();
    $companyName = $ctmDetails[0]->getCompanyName();
    $type = $ctmDetails[0]->getType();
    $reason = $ctmDetails[0]->getReason();
    $remarkB = $ctmDetails[0]->getRemarkB();
    $occupation = $ctmDetails[0]->getOccupation();
    $hobby = $ctmDetails[0]->getHobby();
    $location = $ctmDetails[0]->getLocation();

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $customerPhone."<br>";
    // echo $teleName."<br>";
    // echo $name."<br>";
    // echo $remark."<br>";
    // echo $companyName."<br>";
    // echo $type."<br>";
    // echo $reason."<br>";
    // echo $remarkB."<br>";
    // echo $occupation."<br>";
    // echo $hobby."<br>";
    // echo $location."<br>";

    if(secondFilter($conn,$teleName,$customerName,$customerPhone,$remark,$type,$reason,$companyName,$remarkB,$occupation,$hobby,$location))
    {
        // $_SESSION['messageType'] = 1;
        // header('Location: ../profile.php?type=1');
        echo "<script>alert('Register Success !');window.location='../checkLogGood.php'</script>";    
    }
    else
    {
        echo "<script>alert('FAIL !!');window.location='../checkLogGood.php'</script>";
    }
}
else 
{
    header('Location: ../index.php');
}
?>