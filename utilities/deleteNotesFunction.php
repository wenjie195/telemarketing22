<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Notepad.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $noteUid = $_POST["notepad_uid"];
     $type = 4;

     $notesDetails = getNotepad($conn," uid = ? ",array("uid"),array($noteUid),"s");    

     if(!$notesDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($type)
          {
               array_push($tableName,"type");
               array_push($tableValue,$type);
               $stringType .=  "i";
          }

          array_push($tableValue,$noteUid);
          $stringType .=  "s";
          $updatedNotes = updateDynamicData($conn,"notepad"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updatedNotes)
          {
               echo "<script>alert('notes deleted !!');window.location='../teleSpeechDraft.php'</script>";   
          }
          else
          {
               echo "<script>alert('fail to delete notes !!');window.location='../teleSpeechDraft.php'</script>";   
          }
     }
     else
     {
          echo "<script>alert('ERROR !!');window.location='../teleSpeechDraft.php'</script>";   
     }
}
else 
{
     header('Location: ../index.php');
}
?>