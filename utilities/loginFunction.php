<?php
if (session_id() == "")
{
    session_start();
}

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Emgcsos.php';
require_once dirname(__FILE__) . '/../classes/PageView.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

function addLoginDetails($conn,$uid,$ip_address,$userAgent,$deviceType,$user_browser)
{
    if(insertDynamicData($conn,"pageview",array("user_uid","ip_address","user_agent","device_type","browser_type"),
    array($uid,$ip_address,$userAgent,$deviceType,$user_browser),"sssss") === null)
    {
        echo "gg";
    }
    else{    }
    return true;
}

$conn = connDB();

$userRows = getEmgc($conn," WHERE type = 1 ");
$userDetails = $userRows[0];

// $userRows2 = getEmgc($conn," WHERE type = 8 ");
// $userDetails2 = $userRows2[0];

$conn->close();
?>

<?php
    //whether ip is from share internet
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   
    {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    }
    //whether ip is from proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
    {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    //whether ip is from remote address
    else
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }
    $ip_address;
?>

<?php
    $userAgent=$_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$userAgent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($userAgent,0,4)))
?>

<?php
    function isMobileDevice()
    {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
    if(isMobileDevice())
    {
        $deviceType = "Mobile Browser";
        echo "Mobile Browser Detected";
        echo "<br>";
        echo "<br>";
    }
    else
    {
        $deviceType = NULL;
        echo "Mobile Browser Not Detected";
        echo "<br>";
        echo "<br>";
    }
?>

<?php
    $arr_browsers = ["Opera", "Edg", "Chrome", "Safari", "Firefox", "MSIE", "Trident"];
    $agent = $_SERVER['HTTP_USER_AGENT'];
    $user_browser = '';
    foreach ($arr_browsers as $browser)
    {
        if (strpos($agent, $browser) !== false)
        {
            $user_browser = $browser;
            break;
        }   
    }
    switch ($user_browser)
    {
        case 'MSIE':
        $user_browser = 'Internet Explorer';
        break;

        case 'Trident':
        $user_browser = 'Internet Explorer';
        break;

        case 'Edg':
        $user_browser = 'Microsoft Edge';
        break;
    }
    echo "You are using ".$user_browser." browser";
    echo "<br>";
    echo "<br>";
?>

<?php
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        // $email = rewrite($_POST['email']);

        $username = rewrite($_POST['username']);
        $password = $_POST['password'];

        // $userRows = getUser($conn," WHERE username = ? ",array("username"),array($email),"s");

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {

                    $_SESSION['uid'] = $user->getUid();
                    $uid = $user->getUid();
                    $_SESSION['usertype_level'] = $user->getUserType();

                    if(addLoginDetails($conn,$uid,$ip_address,$userAgent,$deviceType,$user_browser))
                    {
                        // $_SESSION['uid'] = $user->getUid();
                        // $_SESSION['usertype_level'] = $user->getUserType();
                        
                        if($user->getUserType() == 0)
                        {
                            header('Location: ../adminDashboard.php');
                            // echo "admin page";
                        }
                        elseif($user->getUserType() == 2)
                        {
                            echo "<meta http-equiv=Refresh content=1;url=https://bigdomain.my/>";
                            // echo $userDetails2->getLink();
                        }
    
                        elseif($user->getUserType() == 3)
                        {
                            header('Location: ../companyDashboard.php');
                        }
    
                        else
                        {
                            // echo "user page";
                            // header('Location: ../teleDashboard.php');
                            // header('Location: https://bigdomain.my');
                            
                            echo $userDetails->getLink();
                            // header('Location: ../companySelection.php');
                        }
                    }
                    else
                    {
                        echo "fail";
                    }

                    // if(isset($_POST['remember-me'])) 
                    // {
                        
                    //     // setcookie('email-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                    //     // echo 'remember me';
                    // }
                    // else 
                    // {
                    //     // setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                    //     // echo 'null';
                    // }

                    // $_SESSION['uid'] = $user->getUid();
                    // $_SESSION['usertype_level'] = $user->getUserType();
                    
                    // if($user->getUserType() == 0)
                    // {
                    //     header('Location: ../adminDashboard.php');
                    //     // echo "admin page";
                    // }
                    // elseif($user->getUserType() == 2)
                    // {
                    //     echo "<meta http-equiv=Refresh content=1;url=https://bigdomain.my/>";
                    //     // echo $userDetails2->getLink();
                    // }

                    // elseif($user->getUserType() == 3)
                    // {
                    //     header('Location: ../companyDashboard.php');
                    // }

                    // else
                    // {
                    //     // echo "user page";
                    //     // header('Location: ../teleDashboard.php');
                    //     // header('Location: https://bigdomain.my');
                        
                    //     echo $userDetails->getLink();
                    //     // header('Location: ../companySelection.php');
                    // }
                }
                else 
                {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../index.php?type=11');
                    // promptError("Incorrect email or password");
                    // echo "incorrect email or password";
                    echo "<script>alert('incorrect password');window.location='../index.php'</script>";
                }

        }
        else
        {
        //   $_SESSION['messageType'] = 1;
        //   header('Location: ../index.php?type=7');
        //   echo "no user with this email ";
          echo "<script>alert('no user with this username');window.location='../index.php'</script>";
          //   promptError("This account does not exist");
        }
    }

    $conn->close();
}
?>