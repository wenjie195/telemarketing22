<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $id = $_POST["id"];
    $status = $_POST["update_status"];
    $update_remark = $_POST["update_remark"];

    $no_of_call = $_POST["no_of_call"];
    $update_no_of_call = $no_of_call + 1;

    // $update_status = "BAD";

    $tele_username = $_POST["tele_username"];
    $tele_uid = $_POST["tele_uid"];
    $customer_name = $_POST["customer_name"];

    $customer_phoneno = $_POST["customer_phoneno"];

    //additional task
    $update_name = $_POST["update_name"];
    $update_type = $_POST["update_type"];
    // $update_reason = $_POST["update_reason"];
    $Arr = $_POST["arr"];
    $ArrImplode = implode(", ",$Arr); //display array value

    $comp = $_POST["update_company"];
    $remarkB = $_POST["update_remark_two"];
    $occupation = $_POST["update_occupation"];
    $hobby = $_POST["update_hobby"];
    // $location = $_POST["update_location"];
    $location = " - ";

    $recording = $_FILES['file']['name'];

    $target_dir = "../upload_recording/";
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    // Select file type
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Valid file extensions
    // $extensions_arr = array("jpg","jpeg","png","gif");
    // $extensions_arr = array("jpg","jpeg","png","gif","mp3");
    $extensions_arr = array("jpg","jpeg","png","gif","mp3","m4a","wma");

    if( in_array($imageFileType,$extensions_arr) )
    {
    move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$recording);
    }

}

if(isset($_POST['editSubmit']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($status)
    {
        array_push($tableName,"status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }
    if($update_remark)
    {
        array_push($tableName,"remark");
        array_push($tableValue,$update_remark);
        $stringType .=  "s";
    }
    if($update_no_of_call)
    {
        array_push($tableName,"no_of_call");
        array_push($tableValue,$update_no_of_call);
        $stringType .=  "s";
    }
    // if($update_status)
    // {
    //     array_push($tableName,"update_status");
    //     array_push($tableValue,$update_status);
    //     $stringType .=  "s";
    // }
    if($update_name)
    {
        array_push($tableName,"name");
        array_push($tableValue,$update_name);
        $stringType .=  "s";
    }
    if($update_type)
    {
        array_push($tableName,"type");
        array_push($tableValue,$update_type);
        $stringType .=  "s";
    }
    if($ArrImplode)
    {
        array_push($tableName,"reason");
        array_push($tableValue,$ArrImplode);
        $stringType .=  "s";
    }
    if($comp)
    {
        array_push($tableName,"company_name");
        array_push($tableValue,$comp);
        $stringType .=  "s";
    }
    if($recording)
    {
        array_push($tableName,"recording");
        array_push($tableValue,$recording);
        $stringType .=  "s";
    } 
    if($remarkB)
    {
        array_push($tableName,"remark_two");
        array_push($tableValue,$remarkB);
        $stringType .=  "s";
    } 
    if($occupation)
    {
        array_push($tableName,"occupation");
        array_push($tableValue,$occupation);
        $stringType .=  "s";
    } 
    if($hobby)
    {
        array_push($tableName,"hobby");
        array_push($tableValue,$hobby);
        $stringType .=  "s";
    } 
    if($location)
    {
        array_push($tableName,"location");
        array_push($tableValue,$location);
        $stringType .=  "s";
    } 

    array_push($tableValue,$id);
    $stringType .=  "s";
    $updateCustomerDetails = updateDynamicData($conn,"customerdetails"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($updateCustomerDetails)
    {
        // $_SESSION['messageType'] = 1;
        // header('Location: ../teleDashboard.php');

        $uid = $tele_uid;
        $teleName = $tele_username;
        $customerName = $customer_name;
        $updateStatus = $status;
        $updateRemark = $update_remark;
        $noOfuUpdate = $update_no_of_call;

        $customerPhone = $customer_phoneno;

        $type = $update_type;
        $reason = $ArrImplode;
        $companyName = $comp;

        // if (timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName))
        if (timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName,$recording,$remarkB,$occupation,$hobby,$location))
        {   
            // echo "<script>alert('Data Updated !');window.location='../teleDashboardBlack.php'</script>";   
            echo "<script>alert('Data Updated and Stored !');window.location='../teleDashboardBlack.php'</script>"; 
        }
        else
        {
            echo "<script>alert('Error Level 2');window.location='../teleDashboardBlack.php'</script>";  
        }
    }
    else
    {    
        echo "<script>alert('Unable to update data !');window.location='../teleDashboardBlack.php'</script>";      
    }
}
else
{
  header('Location: ../index.php');     
}

// function timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName)
// {
//      if(insertDynamicData($conn,"time_teleupdate",array("uid","tele_name","customer_name","customer_phone","update_status","update_remark","no_of_update","type","reason","company_name"),
//      array($uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName),"ssssssssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }

function timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName,$recording,$remarkB,$occupation,$hobby,$location)
{
     if(insertDynamicData($conn,"time_teleupdate",array("uid","tele_name","customer_name","customer_phone","update_status","update_remark","no_of_update","type","reason","company_name","recording","remark_two","occupation","hobby","location"),
     array($uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName,$recording,$remarkB,$occupation,$hobby,$location),"sssssssssssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

?>